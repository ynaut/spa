var API_ENDPOINT = '/api/tasks';
var PORT = '3005';

var tasks = [];
var req = null;

window.onload = function () {
    console.log('Page loaded, means our script is loaded and we are safe to run it');

    req = new XMLHttpRequest();
    //traceRequest(req);

    //toggleWaiting();
    //getTasksSync(req);
    getTasksAsync(req);
    toggleWaiting();
};

/*
This readyState property chronicles the path your HTTP request takes,
and each change in its value results in the readystatechange event getting fired.
 */
function traceRequest(req) {
    req.onreadystatechange = function () {
        switch (this.readyState) {
            case 0:
                console.log(' readyState UNSENT');
                break;
            case 1:
                console.log(' readyState OPENED');
                break;
            case 2:
                console.log(' readyState HEADERS_RECEIVED');
                break;
            case 3:
                console.log(' readyState LOADING');
                break;
            case 4:
                console.log(' readyState DONE');
                break;
        }
        // if (this.readyState == 4 && this.status == 200) {
        //     tasks = JSON.parse(this.responseText).length;
        //     showServerResponseOnPage();
        // }
    };
    req.open("GET", "http://localhost:" + PORT + API_ENDPOINT, true);
    req.send();
}

/** 
 * By "waiting" we mean on the response from server
*/
function toggleWaiting() {
    var loaders = document.querySelectorAll('.loader');
    loaders.forEach((loader) => {
        loader.classList.toggle('loader--visible');
    })
}

/**
 * Sends an request snchroniously DON'T DO THIS
 * This is only for demonstration purpose, use getTasksAsync() instead
 * 
 * @param {object} req -the XML HttpRequest object
 */
function getTasksSync(req) {
    req.open("GET", "http://localhost:" + PORT + API_ENDPOINT, false);
    req.send();
    tasks = JSON.parse(req.responseText);
    showServerResponseOnPage();
    disableAbortButton();
}

/**
 * Sends an request to the server asynchronously
 * 
 * @param {object} req -the XML HttpRequest object
 */
function getTasksAsync(req) {
    req.addEventListener('load', reqLoaded);
    req.open("GET", "http://localhost:" + PORT + API_ENDPOINT);
    req.send();
}

/**
 * This function will run each time our XML HttpRequest object triggers an load-event
 * 
 * @param {object} e -event that triggered
 */
function reqLoaded(e) {
    if (e.target.status === 200) {
        tasks = JSON.parse(e.target.responseText);
        toggleWaiting();
        showServerResponseOnPage();
        disableAbortButton();
    } else {
        console.log('Failed to get tasks. Server responded with status', e.target.status);
    }
}

/** 
 * Modifies DOM to show data from server on HTML page
*/
function showServerResponseOnPage() {
    var nofTasksElement = document.querySelector('.nof-tasks');
    nofTasksElement.innerHTML = String(tasks.length);
    console.log('HTML updated!');
}

function handleAbortButtonClick(){
    console.log('user aborted httpRequest');
    toggleWaiting(); 
    disableAbortButton();
    req.abort();
}

function disableAbortButton(){
    var abortButton = document.querySelector('.abort-button');
    abortButton.disabled = true; 
}
