var express = require('express');
var path = require('path');
var tasks = require('./tasks.js');

var app = express();
var port = '3005';

// No need for CORS accepting all (*) now that index.html and api is served from the SAME server and port!
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

// Static assets, every file under specified directories will be served. (css, html, js)
app.use('/', express.static(path.join(__dirname, '../public')));
app.use('/clientES5_fetch', express.static(path.join(__dirname, '../clientES5_fetch')));
app.use('/clientES5_httpRequest', express.static(path.join(__dirname, '../clientES5_httpRequest')));
app.use('/client_React', express.static(path.join(__dirname, '../client_React/build')));

// API endpoints. (typically data is returned or a service preformed on server)
app.get('/api/players', function (req, res) {
    //player.ranking + player.name + player.age + player.positionText + player.goal+ player.ranking;
    const players = [
            {ranking: 1, name: "Donald Duck", age: "8", positionText: "forward", goal:12},
            {ranking: 2, name: "Mickey Mouse", age: "14", positionText: "middle", goal:5},
            {ranking: 3, name: "John Doe", age: "34", positionText: "middle", goal:8}
        ];

    res.json(players);
});
app.get('/api/tasks', function (req, res) {
    setTimeout(() => res.json(tasks), 3000);
});


app.listen(port, () => console.log(`Server is listening on port ${port}!`));
