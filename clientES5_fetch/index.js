var API_ENDPOINT = '/api/players';
var PORT = '3005';

var players = [];

window.onload = function () {
    console.log('Page loaded');
    fetchDataFromServer('http://localhost:' + PORT + API_ENDPOINT).then(function (data) {
        players = data;
        addTableToHtml(data);
    }).catch(function(err){
        console.log('ERROR presenting data from server', err);
    });
};

// Create the type of element you pass in the parameters
function createNode(element) {
    return document.createElement(element);
}

// Append the second parameter(element) to the first one
function append(parent, el) {
    return parent.appendChild(el);
}

function fetchDataFromServer(apiUrl) {
    var serverResponse = fetch(apiUrl).then(function (response) {
        if (!response.ok) {
            return 'ERROR';
        }
        return response.json();
    })
    .catch(function (err) {
        console.log('ERROR fetching from server', err);
    });

    return serverResponse;
}

function handleClick(id){
    var player = players[id - 1];
    //var formattedData = 'Name:' + player.name + '\nAge:' + player.age + '\nPosition:' + player.position + '\nGoal:' +player.goal;
    var formattedData =
        'Name:\n' + " " + player.name
        + '\nAge:\n' + " " + player.age
        + '\nPosition:\n' + " " + player.positionText
        + '\nGoal:\n' + " " + player.goal;

    alert(formattedData);
}

function addTableToHtml(data){
    var dataElement = document.querySelector(".data");
    var table = createNode('table');
    var tr = createNode('tr');

    // apply some styling
    // table.style.border = '1px gray solid';
    // table.style.padding = '5px';
    // table.style.margin = '5px';

    // add header row to table
    tr.innerHTML = '<th>Ranking</th>'+'<th>Name</th>'+'<th>Age</th>'+'<th>Position</th>'+'<th>Goal</th>'+'<th>Action</th>';
    append(table, tr);

    // add rest of rows to table
    data.map(function (player) {
        tr = createNode('tr');

        tr.innerHTML = '<td>' + player.ranking + '</td><td>' + player.name + '</td>' + '<td>' + player.age + '</td>'+'<td>'+ player.positionText + '</td>'+'<td>'+ player.goal+'</td>'+'<td>'+'<input type="button" value="View" onclick="handleClick(' + player.ranking + ')"/></td>';

        append(table, tr);
    });

    append(dataElement, table);
}
